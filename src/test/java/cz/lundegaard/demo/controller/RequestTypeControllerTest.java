package cz.lundegaard.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.lundegaard.demo.dto.RequestTypeDto;
import cz.lundegaard.demo.service.RequestTypeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RequestTypeController.class)
class RequestTypeControllerTest {

    private static final long REQUEST_TYPE_ID = 1;
    private static final String NAME = "name";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private RequestTypeService requestTypeService;

    @Test
    void getTriggeredThenServiceIsCalled() throws Exception {
        RequestTypeDto dto = createDto();
        when(requestTypeService.findAll()).thenReturn(Collections.singletonList(dto));
        mockMvc.perform(
                get("/requestType"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id")
                        .value(dto.getId()))
                .andExpect(jsonPath("$[0].name")
                        .value(dto.getName()));
        verify(requestTypeService, times(1)).findAll();
    }

    @Test
    void putTriggeredThenServiceIsCalled() throws Exception {
        RequestTypeDto dto = createDto();
        mockMvc.perform(
                put("/requestType")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(requestTypeService, times(1)).update(any(RequestTypeDto.class));
    }

    @Test
    void postTriggeredThenServiceIsCalled() throws Exception {
        RequestTypeDto dto = createDto();
        mockMvc.perform(
                post("/requestType")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(requestTypeService, times(1)).create(any(RequestTypeDto.class));
    }

    @Test
    void deleteTriggeredThenServiceIsCalled() throws Exception {
        mockMvc.perform(
                delete("/requestType")
                        .param("id", String.valueOf(REQUEST_TYPE_ID)))
                .andExpect(status().isNoContent());
        verify(requestTypeService, times(1)).delete(eq(REQUEST_TYPE_ID));
    }

    private RequestTypeDto createDto() {
        RequestTypeDto requestTypeDto = RequestTypeDto.builder()
                .setId(REQUEST_TYPE_ID)
                .setName(NAME)
                .build();
        return requestTypeDto;
    }
}
