package cz.lundegaard.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.lundegaard.demo.dto.ContactRequestDto;
import cz.lundegaard.demo.service.ClientService;
import cz.lundegaard.demo.service.ContactRequestService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ContactRequestController.class)
class ContactRequestControllerTest {

    private static final long ID = 1;
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String MESSAGE = "message";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ContactRequestService contactRequestService;
    @MockBean
    private ClientService clientService;

    @Test
    void getTriggeredThenServiceIsCalled() throws Exception {
        ContactRequestDto dto = createDto();
        when(contactRequestService.findAll()).thenReturn(Collections.singletonList(dto));
        mockMvc.perform(
                get("/contactRequest"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id")
                        .value(dto.getId()))
                .andExpect(jsonPath("$[0].message")
                        .value(dto.getMessage()))
                .andExpect(jsonPath("$[0].name")
                        .value(dto.getName()))
                .andExpect(jsonPath("$[0].surname")
                        .value(dto.getSurname()));
        verify(contactRequestService, times(1)).findAll();
    }

    @Test
    void putTriggeredThenServiceIsCalled() throws Exception {
        ContactRequestDto dto = createDto();
        mockMvc.perform(
                put("/contactRequest")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(contactRequestService, times(1)).update(any(ContactRequestDto.class));
    }

    @Test
    void postTriggeredThenServiceIsCalled() throws Exception {
        ContactRequestDto dto = createDto();
        mockMvc.perform(
                post("/contactRequest")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(contactRequestService, times(1)).create(any(ContactRequestDto.class));
    }

    @Test
    void deleteTriggeredThenServiceIsCalled() throws Exception {
        mockMvc.perform(
                delete("/contactRequest")
                        .param("id", String.valueOf(ID)))
                .andExpect(status().isNoContent());
        verify(contactRequestService, times(1)).delete(eq(ID));
    }

    @Test
    void contactUsTriggeredThenServiceIsCalled() throws Exception {
        mockMvc.perform(
                post("/contactRequest/contactUs")
                        .param("id", String.valueOf(ID))
                        .param("requestType", String.valueOf(ID))
                        .param("policyNumber", "0123456789")
                        .param("name", NAME)
                        .param("surname", SURNAME)
                        .param("message", MESSAGE)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(clientService, times(1)).findBy(anyString());
    }

    private ContactRequestDto createDto() {
        ContactRequestDto contactRequestDto = ContactRequestDto.builder()
                .setId(ID)
                .setName(NAME)
                .setSurname(SURNAME)
                .setMessage(MESSAGE)
                .build();
        return contactRequestDto;
    }
}