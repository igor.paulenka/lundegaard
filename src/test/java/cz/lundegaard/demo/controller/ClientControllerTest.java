package cz.lundegaard.demo.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import cz.lundegaard.demo.dto.ClientDto;
import cz.lundegaard.demo.service.ClientService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(ClientController.class)
class ClientControllerTest {

    private static final long CLIENT_ID = 1;
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String POLICY_NUMBER = "0123456789";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ClientService clientService;

    @Test
    void getTriggeredThenServiceIsCalled() throws Exception {
        ClientDto dto = createDto();
        when(clientService.findAll()).thenReturn(Collections.singletonList(dto));
        mockMvc.perform(
                get("/client"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id")
                        .value(dto.getId()))
                .andExpect(jsonPath("$[0].policyNumber")
                        .value(dto.getPolicyNumber()))
                .andExpect(jsonPath("$[0].name")
                        .value(dto.getName()))
                .andExpect(jsonPath("$[0].surname")
                        .value(dto.getSurname()));
        verify(clientService, times(1)).findAll();
    }

    @Test
    void putTriggeredThenServiceIsCalled() throws Exception {
        ClientDto dto = createDto();
        mockMvc.perform(
                put("/client")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(clientService, times(1)).update(any(ClientDto.class));
    }

    @Test
    void postTriggeredThenServiceIsCalled() throws Exception {
        ClientDto dto = createDto();
        mockMvc.perform(
                post("/client")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(clientService, times(1)).create(any(ClientDto.class));
    }

    @Test
    void deleteTriggeredThenServiceIsCalled() throws Exception {
        mockMvc.perform(
                delete("/client")
                        .param("id", String.valueOf(CLIENT_ID)))
                .andExpect(status().isNoContent());
        verify(clientService, times(1)).delete(eq(CLIENT_ID));
    }

    private ClientDto createDto() {
        ClientDto clientDto = ClientDto.builder()
                .setId(CLIENT_ID)
                .setName(NAME)
                .setSurname(SURNAME)
                .setPolicyNumber(POLICY_NUMBER)
                .build();
        return clientDto;
    }
}