package cz.lundegaard.demo.service;

import cz.lundegaard.demo.domain.ContactRequest;
import cz.lundegaard.demo.dto.ContactRequestDto;
import cz.lundegaard.demo.dto.mapping.ContactRequestMapper;
import cz.lundegaard.demo.repository.ContactRequestRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

class ContactRequestServiceImplTest {

    private ContactRequestService contactRequestService;

    @Mock
    private ContactRequestRepository contactRequestRepository;
    @Mock
    private ContactRequestMapper contactRequestMapper;

    private ContactRequest testingEntity;
    private ContactRequestDto testingDto;
    private static long ID = 1;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        contactRequestService = new ContactRequestServiceImpl(contactRequestRepository, contactRequestMapper);
        testingEntity = new ContactRequest();
        testingDto = ContactRequestDto.builder().build();
        Mockito.validateMockitoUsage();
    }

    @Test
    void findAllCallRepositoryOnce() {
        when(contactRequestRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        contactRequestService.findAll();
        verify(contactRequestRepository, times(1)).findAll();
    }

    @Test
    void findAllCallMapperOnce() {
        when(contactRequestMapper.toDto(testingEntity)).thenReturn(testingDto);
        when(contactRequestRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        contactRequestService.findAll();
        verify(contactRequestMapper, times(1)).toDto(testingEntity);
    }

    @Test
    void createCallRepository() {
        when(contactRequestMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(contactRequestRepository.create(testingEntity)).thenReturn(testingEntity);
        contactRequestService.create(testingDto);
        verify(contactRequestRepository, times(1)).create(testingEntity);
    }

    @Test
    void createCallMapper() {
        when(contactRequestMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(contactRequestRepository.create(testingEntity)).thenReturn(testingEntity);
        contactRequestService.create(testingDto);
        verify(contactRequestMapper, times(1)).toDto(testingEntity);
        verify(contactRequestMapper, times(1)).toEntity(testingDto);
    }

    @Test
    void createIsTriggered_thenEntityIsCreated() {
        when(contactRequestMapper.toEntity(testingDto)).thenReturn(testingEntity);
        contactRequestService.create(testingDto);

        ArgumentCaptor<ContactRequest> argument = ArgumentCaptor.forClass(ContactRequest.class);
        verify(contactRequestRepository).create(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(ContactRequest.class);
    }

    @Test
    void updateCallRepository() {
        when(contactRequestMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(contactRequestRepository.update(testingEntity)).thenReturn(testingEntity);
        contactRequestService.update(testingDto);
        verify(contactRequestRepository, times(1)).update(testingEntity);
    }

    @Test
    void updateCallMapper() {
        when(contactRequestMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(contactRequestRepository.update(testingEntity)).thenReturn(testingEntity);
        contactRequestService.update(testingDto);
        verify(contactRequestMapper, times(1)).toEntity(testingDto);
    }

    @Test
    void updateIsTriggered_thenEntityIsUpdate() {
        when(contactRequestMapper.toEntity(testingDto)).thenReturn(testingEntity);
        contactRequestService.update(testingDto);

        ArgumentCaptor<ContactRequest> argument = ArgumentCaptor.forClass(ContactRequest.class);
        verify(contactRequestRepository).update(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(ContactRequest.class);
    }

    @Test
    void deleteCallRepository() {
        contactRequestService.delete(ID);
        verify(contactRequestRepository, times(1)).delete(ID);
    }

    @Test
    void deleteIsTriggered_thenEntityIsDeleted() {
        contactRequestService.delete(ID);
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(contactRequestRepository).delete(argument.capture());
        assertThat(argument.getValue()).isEqualTo(ID);
    }


}