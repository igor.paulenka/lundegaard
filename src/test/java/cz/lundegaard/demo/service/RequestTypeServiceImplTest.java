package cz.lundegaard.demo.service;

import cz.lundegaard.demo.domain.RequestType;
import cz.lundegaard.demo.dto.RequestTypeDto;
import cz.lundegaard.demo.dto.mapping.RequestTypeMapper;
import cz.lundegaard.demo.repository.RequestTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

class RequestTypeServiceImplTest {

    private RequestTypeService requestTypeService;

    @Mock
    private RequestTypeRepository requestTypeRepository;
    @Mock
    private RequestTypeMapper requestTypeMapper;

    private RequestType testingEntity;
    private RequestTypeDto testingDto;
    private static long ID = 1;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        requestTypeService = new RequestTypeServiceImpl(requestTypeRepository, requestTypeMapper);
        testingEntity = new RequestType();
        testingDto = RequestTypeDto.builder().build();
        Mockito.validateMockitoUsage();
    }

    @Test
    void findAllCallRepositoryOnce() {
        when(requestTypeRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        requestTypeService.findAll();
        verify(requestTypeRepository, times(1)).findAll();
    }

    @Test
    void findAllCallMapperOnce() {
        when(requestTypeMapper.toDto(testingEntity)).thenReturn(testingDto);
        when(requestTypeRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        requestTypeService.findAll();
        verify(requestTypeMapper, times(1)).toDto(testingEntity);
    }

    @Test
    void createCallRepository() {
        when(requestTypeMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(requestTypeRepository.create(testingEntity)).thenReturn(testingEntity);
        requestTypeService.create(testingDto);
        verify(requestTypeRepository, times(1)).create(testingEntity);
    }

    @Test
    void createCallMapper() {
        when(requestTypeMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(requestTypeRepository.create(testingEntity)).thenReturn(testingEntity);
        requestTypeService.create(testingDto);
        verify(requestTypeMapper, times(1)).toDto(testingEntity);
        verify(requestTypeMapper, times(1)).toEntity(testingDto);
    }

    @Test
    void createIsTriggered_thenEntityIsCreated() {
        when(requestTypeMapper.toEntity(testingDto)).thenReturn(testingEntity);
        requestTypeService.create(testingDto);

        ArgumentCaptor<RequestType> argument = ArgumentCaptor.forClass(RequestType.class);
        verify(requestTypeRepository).create(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(RequestType.class);
    }

    @Test
    void updateCallRepository() {
        when(requestTypeMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(requestTypeRepository.update(testingEntity)).thenReturn(testingEntity);
        requestTypeService.update(testingDto);
        verify(requestTypeRepository, times(1)).update(testingEntity);
    }

    @Test
    void updateCallMapper() {
        when(requestTypeMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(requestTypeRepository.update(testingEntity)).thenReturn(testingEntity);
        requestTypeService.update(testingDto);
        verify(requestTypeMapper, times(1)).toEntity(testingDto);
    }

    @Test
    void updateIsTriggered_thenEntityIsUpdate() {
        when(requestTypeMapper.toEntity(testingDto)).thenReturn(testingEntity);
        requestTypeService.update(testingDto);

        ArgumentCaptor<RequestType> argument = ArgumentCaptor.forClass(RequestType.class);
        verify(requestTypeRepository).update(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(RequestType.class);
    }

    @Test
    void deleteCallRepository() {
        requestTypeService.delete(ID);
        verify(requestTypeRepository, times(1)).delete(ID);
    }

    @Test
    void deleteIsTriggered_thenEntityIsDeleted() {
        requestTypeService.delete(ID);
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(requestTypeRepository).delete(argument.capture());
        assertThat(argument.getValue()).isEqualTo(ID);
    }
}