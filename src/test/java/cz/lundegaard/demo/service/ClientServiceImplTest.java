package cz.lundegaard.demo.service;

import cz.lundegaard.demo.domain.Client;
import cz.lundegaard.demo.dto.ClientDto;
import cz.lundegaard.demo.dto.mapping.ClientMapper;
import cz.lundegaard.demo.repository.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

class ClientServiceImplTest {

    private ClientService clientService;

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ClientMapper clientMapper;

    private Client testingEntity;
    private ClientDto testingDto;
    private static long ID = 1;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        clientService = new ClientServiceImpl(clientRepository, clientMapper);
        testingEntity = new Client();
        testingDto = ClientDto.builder().build();
        Mockito.validateMockitoUsage();
    }

    @Test
    void findAllCallRepositoryOnce() {
        when(clientRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        clientService.findAll();
        verify(clientRepository, times(1)).findAll();
    }

    @Test
    void findAllCallMapperOnce() {
        when(clientMapper.toDto(testingEntity)).thenReturn(testingDto);
        when(clientRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        clientService.findAll();
        verify(clientMapper, times(1)).toDto(testingEntity);
    }

    @Test
    void createCallRepository() {
        when(clientMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(clientRepository.create(testingEntity)).thenReturn(testingEntity);
        clientService.create(testingDto);
        verify(clientRepository, times(1)).create(testingEntity);
    }

    @Test
    void createCallMapper() {
        when(clientMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(clientRepository.create(testingEntity)).thenReturn(testingEntity);
        clientService.create(testingDto);
        verify(clientMapper, times(1)).toDto(testingEntity);
        verify(clientMapper, times(1)).toEntity(testingDto);
    }

    @Test
    void createIsTriggered_thenEntityIsCreated() {
        when(clientMapper.toEntity(testingDto)).thenReturn(testingEntity);
        clientService.create(testingDto);

        ArgumentCaptor<Client> argument = ArgumentCaptor.forClass(Client.class);
        verify(clientRepository).create(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(Client.class);
    }

    @Test
    void updateCallRepository() {
        when(clientMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(clientRepository.update(testingEntity)).thenReturn(testingEntity);
        clientService.update(testingDto);
        verify(clientRepository, times(1)).update(testingEntity);
    }

    @Test
    void updateCallMapper() {
        when(clientMapper.toEntity(testingDto)).thenReturn(testingEntity);
        when(clientRepository.update(testingEntity)).thenReturn(testingEntity);
        clientService.update(testingDto);
        verify(clientMapper, times(1)).toEntity(testingDto);
    }

    @Test
    void updateIsTriggered_thenEntityIsUpdate() {
        when(clientMapper.toEntity(testingDto)).thenReturn(testingEntity);
        clientService.update(testingDto);

        ArgumentCaptor<Client> argument = ArgumentCaptor.forClass(Client.class);
        verify(clientRepository).update(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(Client.class);
    }

    @Test
    void deleteCallRepository() {
        clientService.delete(ID);
        verify(clientRepository, times(1)).delete(ID);
    }

    @Test
    void deleteIsTriggered_thenEntityIsDeleted() {
        clientService.delete(ID);
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(clientRepository).delete(argument.capture());
        assertThat(argument.getValue()).isEqualTo(ID);
    }

    @Test
    void findByCallRepository() {
        clientService.findBy(anyString());
        verify(clientRepository, times(1)).findBy(anyString());
    }
}