package cz.lundegaard.demo.dto.mapping;

import cz.lundegaard.demo.domain.Client;
import cz.lundegaard.demo.dto.ClientDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = ClientMapperImpl.class)
class ClientMapperImplTest {

    @Autowired
    private ClientMapper clientMapper;

    private Client testingEntity;
    private ClientDto testingDto;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        testingEntity = new Client();
        testingDto = ClientDto.builder()
                .setId(1L)
                .setPolicyNumber("0123456789")
                .setName("Name")
                .setSurname("Surname")
                .build();
    }

    @Test
    void mapperConvertEntityToDto() {
        assertThat(clientMapper.toDto(testingEntity)).isExactlyInstanceOf(ClientDto.class);
    }

    @Test
    void mapperConvertDtoToEntity() {
        assertThat(clientMapper.toEntity(testingDto)).isExactlyInstanceOf(Client.class);
    }

    @Test
    void dtoHasSameValuesAsEntity() {
        ClientDto dto = clientMapper.toDto(testingEntity);
        assertThat(testingEntity).isEqualToComparingOnlyGivenFields(dto,
                "id",
                "name",
                "surname",
                "policyNumber");
    }

    @Test
    void EntityHasSameValuesAsDto() {
        Client entity = clientMapper.toEntity(testingDto);
        assertThat(entity).isEqualToComparingOnlyGivenFields(testingDto,
                "id",
                "name",
                "surname",
                "policyNumber");
    }
}
