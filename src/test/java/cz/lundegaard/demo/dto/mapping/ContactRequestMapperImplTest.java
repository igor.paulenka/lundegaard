package cz.lundegaard.demo.dto.mapping;

import cz.lundegaard.demo.domain.ContactRequest;
import cz.lundegaard.demo.dto.ContactRequestDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = ContactRequestMapperImpl.class)
class ContactRequestMapperImplTest {

    @Autowired
    private ContactRequestMapper contactRequestMapper;

    private ContactRequest testingEntity;
    private ContactRequestDto testingDto;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        testingEntity = new ContactRequest();
        testingDto = ContactRequestDto.builder()
                .setId(1L)
                .setName("Name")
                .setSurname("Surname")
                .setMessage("Message")
                .build();
    }

    @Test
    void mapperConvertEntityToDto() {
        assertThat(contactRequestMapper.toDto(testingEntity)).isExactlyInstanceOf(ContactRequestDto.class);
    }

    @Test
    void mapperConvertDtoToEntity() {
        assertThat(contactRequestMapper.toEntity(testingDto)).isExactlyInstanceOf(ContactRequest.class);
    }

    @Test
    void dtoHasSameValuesAsEntity() {
        ContactRequestDto dto = contactRequestMapper.toDto(testingEntity);
        assertThat(testingEntity).isEqualToComparingOnlyGivenFields(dto,
                "id",
                "name",
                "surname",
                "message");
    }

    @Test
    void EntityHasSameValuesAsDto() {
        ContactRequest entity = contactRequestMapper.toEntity(testingDto);
        assertThat(entity).isEqualToComparingOnlyGivenFields(testingDto,
                "id",
                "name",
                "surname",
                "message");
    }
}
