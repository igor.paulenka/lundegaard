package cz.lundegaard.demo.dto.mapping;

import cz.lundegaard.demo.domain.RequestType;
import cz.lundegaard.demo.dto.RequestTypeDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = RequestTypeMapperImpl.class)
class RequestTypeMapperImplTest {

    @Autowired
    private RequestTypeMapper requestTypeMapper;

    private RequestType testingEntity;
    private RequestTypeDto testingDto;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        testingEntity = new RequestType();
        testingDto = RequestTypeDto.builder()
                .setId(1L)
                .setName("name")
                .build();
    }

    @Test
    void mapperConvertEntityToDto() {
        assertThat(requestTypeMapper.toDto(testingEntity)).isExactlyInstanceOf(RequestTypeDto.class);
    }

    @Test
    void mapperConvertDtoToEntity() {
        assertThat(requestTypeMapper.toEntity(testingDto)).isExactlyInstanceOf(RequestType.class);
    }

    @Test
    void dtoHasSameValuesAsEntity() {
        RequestTypeDto dto = requestTypeMapper.toDto(testingEntity);
        assertThat(testingEntity).isEqualToComparingOnlyGivenFields(dto,
                "id",
                "name");
    }

    @Test
    void EntityHasSameValuesAsDto() {
        RequestType entity = requestTypeMapper.toEntity(testingDto);
        assertThat(entity).isEqualToComparingOnlyGivenFields(testingDto,
                "id",
                "name");
    }
}
