package cz.lundegaard.demo.repository;

import cz.lundegaard.demo.domain.ContactRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ActiveProfiles("it")
@SpringBootTest
@Transactional
class ContactRequestRepositoryIT {

    private static final long EXISTING_ID = 1;
    private static final long NON_EXISTING_ID = 999999;

    @Autowired
    ContactRequestRepository contactRequestRepository;

    @Test
    void findAllReturnsAll() {
        final int returnedContactRequests = 2;
        final String contactRequest1 = "FirstName";
        final String contactRequest2 = "SecondName";
        List<ContactRequest> contactRequestList = contactRequestRepository.findAll();
        assertThat(contactRequestList).hasSize(returnedContactRequests);

        assertThat(contactRequestList).satisfiesAnyOf(
                name -> assertThat(name.get(0).getName()).isEqualTo(contactRequest1),
                name -> assertThat(name.get(0).getName()).isEqualTo(contactRequest2));
        assertThat(contactRequestList).satisfiesAnyOf(
                name -> assertThat(name.get(1).getName()).isEqualTo(contactRequest1),
                name -> assertThat(name.get(1).getName()).isEqualTo(contactRequest2));

        assertThat(contactRequestList.get(0).getName())
                .isNotEqualTo(contactRequestList.get(1).getName())
                .isNotNull();
    }

    @Test
    void createSaveContactRequestAndGenerateId() {
        ContactRequest newContactRequest = createContactRequest();
        ContactRequest createdContactRequest = contactRequestRepository.create(newContactRequest);
        assertThat(createdContactRequest.getName()).isEqualTo(newContactRequest.getName());
        assertThat(createdContactRequest.getId()).isEqualTo(newContactRequest.getId());
    }

    @Test
    void createThrowExceptionWhenNotNullValuePolicyViolation() {
        ContactRequest newContactRequest = new ContactRequest();
        newContactRequest.setName(null);
        assertThatThrownBy(() -> contactRequestRepository.create(newContactRequest))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void updateMethodUpdateContactRequest() {
        ContactRequest oldContactRequest = createContactRequest();
        oldContactRequest.setId(EXISTING_ID);
        ContactRequest newContactRequest = contactRequestRepository.update(oldContactRequest);
        assertThat(newContactRequest.getName()).isEqualTo(createContactRequest().getName());
    }

    @Test
    void updateThrowExceptionWhenNotNullValuePolicyViolation() {
        ContactRequest newContactRequest = new ContactRequest();
        newContactRequest.setSurname(null);
        assertThatThrownBy(() -> contactRequestRepository.update(newContactRequest))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void deleteByIdDeleteContactRequest() {
        List<ContactRequest> oldList = contactRequestRepository.findAll();
        contactRequestRepository.delete(EXISTING_ID);
        List<ContactRequest> newList = contactRequestRepository.findAll();
        assertThat(newList).hasSize((oldList.size()) - 1);
    }

    @Test
    void deleteThrowExceptionWhenContactRequestDoNotExist() {
        assertThatThrownBy(() -> contactRequestRepository.delete(NON_EXISTING_ID))
                .isExactlyInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    private ContactRequest createContactRequest() {
        ContactRequest contactRequest = new ContactRequest();
        contactRequest.setName("name");
        contactRequest.setMessage("message");
        contactRequest.setSurname("surname");
        return contactRequest;
    }
}