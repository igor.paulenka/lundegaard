package cz.lundegaard.demo.repository;

import cz.lundegaard.demo.domain.RequestType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ActiveProfiles("it")
@SpringBootTest
@Transactional
class RequestTypeRepositoryIT {

    private static final long EXISTING_ID = 1;
    private static final long NON_EXISTING_ID = 9999999;

    @Autowired
    RequestTypeRepository requestTypeRepository;

    @Test
    void findAllReturnsAll() {
        final int returnedRequestTypes = 3;
        final String requestType1 = "Contract Adjustment";
        final String requestType2 = "Damage Case";
        final String requestType3 = "Complaint";
        List<RequestType> requestTypeList = requestTypeRepository.findAll();
        assertThat(requestTypeList).hasSize(returnedRequestTypes);

        assertThat(requestTypeList).satisfiesAnyOf(
                name -> assertThat(name.get(0).getName()).isEqualTo(requestType1),
                name -> assertThat(name.get(0).getName()).isEqualTo(requestType2),
                name -> assertThat(name.get(0).getName()).isEqualTo(requestType3));

        assertThat(requestTypeList).satisfiesAnyOf(
                name -> assertThat(name.get(1).getName()).isEqualTo(requestType1),
                name -> assertThat(name.get(1).getName()).isEqualTo(requestType2),
                name -> assertThat(name.get(1).getName()).isEqualTo(requestType3));

        assertThat(requestTypeList).satisfiesAnyOf(
                name -> assertThat(name.get(2).getName()).isEqualTo(requestType1),
                name -> assertThat(name.get(2).getName()).isEqualTo(requestType2),
                name -> assertThat(name.get(2).getName()).isEqualTo(requestType3));

        assertThat(requestTypeList.get(0).getName())
                .isNotEqualTo(requestTypeList.get(1).getName())
                .isNotEqualTo(requestTypeList.get(2).getName())
                .isNotNull();
    }

    @Test
    void createSaveRequestTypeAndGenerateId() {
        RequestType newRequestType = createRequestType();
        RequestType createdRequestType = requestTypeRepository.create(newRequestType);
        assertThat(createdRequestType.getId()).isNotNull();
        assertThat(createdRequestType.getName()).isEqualTo(newRequestType.getName());
        assertThat(createdRequestType.getId()).isEqualTo(newRequestType.getId());
    }

    @Test
    void createThrowExceptionWhenNotNullValuePolicyViolation() {
        RequestType newRequestType = createRequestType();
        newRequestType.setName(null);
        assertThatThrownBy(() -> requestTypeRepository.create(newRequestType))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void updateMethodUpdateRequestType() {
        final int returnedRequestTypes = 3;
        RequestType oldRequestType = createRequestType();
        oldRequestType.setId(EXISTING_ID);
        RequestType newRequestType = requestTypeRepository.update(oldRequestType);
        assertThat(requestTypeRepository.findAll()).hasSize(returnedRequestTypes);
        assertThat(newRequestType.getName()).isEqualTo(createRequestType().getName());
    }

    @Test
    void deleteByIdDeleteRequestType() {
        RequestType requestType = requestTypeRepository.create(createRequestType());
        List<RequestType> oldList = requestTypeRepository.findAll();
        requestTypeRepository.delete(requestType.getId());
        List<RequestType> newList = requestTypeRepository.findAll();
        assertThat(newList).hasSize((oldList.size()) - 1);
    }

    @Test
    void deleteThrowExceptionWhenRequestTypeDoNotExist() {
        assertThatThrownBy(() -> requestTypeRepository.delete(NON_EXISTING_ID))
                .isExactlyInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    private RequestType createRequestType() {
        RequestType requestType = new RequestType();
        requestType.setName("name");
        return requestType;
    }
}