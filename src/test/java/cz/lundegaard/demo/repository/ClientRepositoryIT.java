package cz.lundegaard.demo.repository;

import cz.lundegaard.demo.domain.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ActiveProfiles("it")
@SpringBootTest
@Transactional
class ClientRepositoryIT {

    private static final long EXISTING_ID = 1;
    private static final long NON_EXISTING_ID = 9999999;

    @Autowired
    ClientRepository clientRepository;

    @Test
    void findAllReturnsAll() {
        final int returnedClients = 2;
        final String client1 = "Anthony";
        final String client2 = "Brian";
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(returnedClients);

        assertThat(clientList).satisfiesAnyOf(
                name -> assertThat(name.get(0).getName()).isEqualTo(client1),
                name -> assertThat(name.get(0).getName()).isEqualTo(client2));
        assertThat(clientList).satisfiesAnyOf(
                name -> assertThat(name.get(1).getName()).isEqualTo(client1),
                name -> assertThat(name.get(1).getName()).isEqualTo(client2));
        assertThat(clientList.get(0).getName()).isNotEqualTo(clientList.get(1).getName());
        assertThat(clientList.get(0).getId()).isNotEqualTo(clientList.get(1).getId()).isNotNull();
    }

    @Test
    void createSaveClientAndGenerateId() {
        Client newClient = createClient();
        Client createdClient = clientRepository.create(newClient);
        assertThat(createdClient.getId()).isNotNull();
        assertThat(createdClient.getName()).isEqualTo(newClient.getName());
        assertThat(createdClient.getId()).isEqualTo(newClient.getId());
    }

    @Test
    void createThrowExceptionWhenDuplicatedData() {
        Client newClient = new Client();
        newClient.setName("AnotherName");
        newClient.setPolicyNumber("0123456789");
        newClient.setSurname("anotherSurname");
        assertThatThrownBy(() -> clientRepository.create(newClient))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void createThrowExceptionWhenNotNullValuePolicyViolation() {
        Client newClient = new Client();
        newClient.setPolicyNumber(null);
        assertThatThrownBy(() -> clientRepository.create(newClient))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void updateMethodUpdateClient() {
        final int returnedClients = 2;
        Client oldClient = createClient();
        oldClient.setId(EXISTING_ID);
        Client newClient = clientRepository.update(oldClient);
        assertThat(clientRepository.findAll()).hasSize(returnedClients);
        assertThat(newClient.getName()).isEqualTo(createClient().getName());
    }

    @Test
    void deleteByIdDeleteClient() {
        Client client = clientRepository.create(createClient());
        List<Client> oldList = clientRepository.findAll();
        clientRepository.delete(client.getId());
        List<Client> newList = clientRepository.findAll();
        assertThat(newList).hasSize((oldList.size()) - 1);
    }

    @Test
    void deleteThrowExceptionWhenClientDoNotExist() {
        assertThatThrownBy(() -> clientRepository.delete(NON_EXISTING_ID))
                .isExactlyInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    @Test
    void findByReturnId() {
        final String request = "0123456789";
        long id = clientRepository.findBy(request);
        assertThat(id).isEqualTo(EXISTING_ID).isNotNull();
    }


    private Client createClient() {
        Client client = new Client();
        client.setName("name");
        client.setPolicyNumber("1236547890");
        client.setSurname("surname");
        return client;
    }
}