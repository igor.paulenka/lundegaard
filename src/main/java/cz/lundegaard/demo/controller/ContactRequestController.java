package cz.lundegaard.demo.controller;

import cz.lundegaard.demo.dto.ClientDto;
import cz.lundegaard.demo.dto.ContactRequestDto;
import cz.lundegaard.demo.dto.RequestTypeDto;
import cz.lundegaard.demo.service.ClientService;
import cz.lundegaard.demo.service.ContactRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contactRequest")
public class ContactRequestController {

    private final ContactRequestService contactRequestService;
    private final ClientService clientService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactRequestController.class);

    public ContactRequestController(ContactRequestService contactRequestService, ClientService clientService) {
        this.contactRequestService = contactRequestService;
        this.clientService = clientService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ContactRequestDto> findAll() {
        LOGGER.info("Endpoint find all contact requests was called.");
        List<ContactRequestDto> contactRequestDtoList = contactRequestService.findAll();
        LOGGER.info("Returned was {} contact requests.", contactRequestDtoList.size());
        return contactRequestDtoList;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ContactRequestDto updateContactRequest(@RequestBody ContactRequestDto contactRequest) {
        LOGGER.info("Endpoint update contact requests was called on contact requests with ID: {}.", contactRequest.getId());
        ContactRequestDto newContactRequest = contactRequestService.update(contactRequest);
        LOGGER.info("Contact requests with ID: {} was updated to {}.", contactRequest.getId(), newContactRequest);
        return newContactRequest;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ContactRequestDto createContactRequest(@RequestBody ContactRequestDto contactRequest) {
        LOGGER.info("Endpoint create contact requests was called.");
        ContactRequestDto contactRequestDto = contactRequestService.create(contactRequest);
        LOGGER.info("Contact request {} was created.", contactRequestDto);
        return contactRequestDto;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteContactRequest(@RequestParam("id") long id) {
        LOGGER.info("Endpoint delete contact requests with ID: {} was called.", id);
        contactRequestService.delete(id);
        LOGGER.info("Contact request with ID: {} was deleted.", id);
    }


    @PostMapping("/contactUs")
    @ResponseStatus(HttpStatus.CREATED)
    public void contactUs(@RequestParam String requestType, @RequestParam String policyNumber,
                          @RequestParam String name, @RequestParam String surname, @RequestParam String message) {
        LOGGER.info("Endpoint create contact us request was called.");
        long id = clientService.findBy(policyNumber);
        ClientDto clientDto = ClientDto.builder().setId(id).build();
        RequestTypeDto requestTypeDto = RequestTypeDto.builder().setId(Long.valueOf(requestType)).build();
        ContactRequestDto newRequest = ContactRequestDto.builder()
                .setRequestTypeDto(requestTypeDto)
                .setClientDto(clientDto)
                .setName(name)
                .setSurname(surname)
                .setMessage(message)
                .build();
        contactRequestService.create(newRequest);
        LOGGER.info("Request contact us {} was created.", newRequest);
    }
}
