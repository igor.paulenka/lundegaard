package cz.lundegaard.demo.controller;

import cz.lundegaard.demo.dto.ClientDto;
import cz.lundegaard.demo.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {

    private final ClientService clientService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ClientDto> findAll() {
        LOGGER.info("Endpoint find all clients was called.");
        List<ClientDto> clientDtoList = clientService.findAll();
        LOGGER.info("Returned was {} clients.", clientDtoList.size());
        return clientDtoList;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ClientDto updateClient(@RequestBody ClientDto client) {
        LOGGER.info("Endpoint update client was called on client with ID: {}.", client.getId());
        ClientDto newClient = clientService.update(client);
        LOGGER.info("Client with ID: {} was updated to {}.", client.getId(), newClient);
        return newClient;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClientDto createClient(@RequestBody ClientDto client) {
        LOGGER.info("Endpoint create client was called.");
        ClientDto clientDto = clientService.create(client);
        LOGGER.info("Client {} was created.", clientDto);
        return clientDto;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteClient(@RequestParam("id") long id) {
        LOGGER.info("Endpoint delete client with ID: {} was called.", id);
        clientService.delete(id);
        LOGGER.info("Client with ID: {} was deleted.", id);
    }
}
