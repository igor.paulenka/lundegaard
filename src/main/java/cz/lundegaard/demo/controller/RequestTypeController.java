package cz.lundegaard.demo.controller;

import cz.lundegaard.demo.dto.RequestTypeDto;
import cz.lundegaard.demo.service.RequestTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/requestType")
public class RequestTypeController {

    private final RequestTypeService requestTypeService;

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestTypeController.class);

    public RequestTypeController(RequestTypeService requestTypeService) {
        this.requestTypeService = requestTypeService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<RequestTypeDto> findAll() {
        LOGGER.info("Endpoint find all request types was called.");
        List<RequestTypeDto> requestTypeDtoList = requestTypeService.findAll();
        LOGGER.info("Returned was {} request types.", requestTypeDtoList.size());
        return requestTypeDtoList;
    }

    @GetMapping("/name")
    @ResponseStatus(HttpStatus.OK)
    public List<RequestTypeDto> findAllName(Model model) {
        LOGGER.info("Endpoint find all request types was called.");
        List<RequestTypeDto> requestTypeName = requestTypeService.findAll();
        model.addAttribute("requestTypes", requestTypeName);
        LOGGER.info("Returned was {} request types.", requestTypeName.size());
        return requestTypeName;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public RequestTypeDto updateRequestType(@RequestBody RequestTypeDto requestType) {
        LOGGER.info("Endpoint update request type was called on request type with ID: {}.", requestType.getId());
        RequestTypeDto newRequestType = requestTypeService.update(requestType);
        LOGGER.info("Request type with ID: {} was updated to {}.", requestType.getId(), newRequestType);
        return newRequestType;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RequestTypeDto createRequestType(@RequestBody RequestTypeDto requestType) {
        LOGGER.info("Endpoint create request type was called.");
        RequestTypeDto requestTypeDto = requestTypeService.create(requestType);
        LOGGER.info("Request type {} was created.", requestTypeDto);
        return requestTypeDto;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRequestType(@RequestParam("id") long id) {
        LOGGER.info("Endpoint delete request type with ID: {} was called.", id);
        requestTypeService.delete(id);
        LOGGER.info("Request type with ID: {} was deleted.", id);
    }
}
