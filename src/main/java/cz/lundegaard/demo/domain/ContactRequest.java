package cz.lundegaard.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "CONTACT_REQUEST")
@NamedQuery(name = ContactRequest.QUERY_ALL, query = "SELECT cr FROM ContactRequest cr")
public class ContactRequest extends AbstractEntity {

    public static final String QUERY_ALL = "query.ContactRequest.all";

    @ManyToOne(optional = false)
    @JoinColumn(name = "CLIENT_ID", foreignKey = @ForeignKey(name = "CONTACT_REQUEST_CLIENT_FK"))
    private Client client;
    @Column(name = "MESSAGE", nullable = false, length = 2500)
    private String message;
    @Column(name = "NAME", nullable = false, length = 20)
    private String name;
    @Column(name = "SURNAME", nullable = false, length = 20)
    private String surname;
    @Column(name = "STATUS", nullable = false)
    private boolean status;
    @OneToOne(optional = false)
    @JoinColumn(name = "REQUEST_TYPE_ID", foreignKey = @ForeignKey(name = "CONTACT_REQUEST_REQUEST_TYPE_FK"))
    private RequestType requestType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }
}
