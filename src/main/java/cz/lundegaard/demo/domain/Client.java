package cz.lundegaard.demo.domain;

import javax.persistence.*;
import java.util.List;

@Table(name = "CLIENT")
@Entity
@NamedQuery(name = Client.QUERY_ALL, query = "SELECT c FROM Client c")
public class Client extends AbstractEntity {

    public static final String QUERY_ALL = "query.Client.all";

    @Column(name = "POLICY_NUMBER", nullable = false, length = 10)
    private String policyNumber;
    @Column(name = "NAME", nullable = false, length = 20)
    private String name;
    @Column(name = "SURNAME", nullable = false, length = 20)
    private String surname;
    @OneToMany(mappedBy = "client")
    private List<ContactRequest> contactRequest;

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<ContactRequest> getContactRequest() {
        return contactRequest;
    }

    public void setContactRequest(List<ContactRequest> contactRequest) {
        this.contactRequest = contactRequest;
    }
}
