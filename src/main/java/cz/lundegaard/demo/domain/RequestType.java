package cz.lundegaard.demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "REQUEST_TYPE")
@NamedQuery(name = RequestType.QUERY_ALL, query = "SELECT rt FROM RequestType rt")
public class RequestType extends AbstractEntity {

    public static final String QUERY_ALL = "query.RequestType.all";

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
