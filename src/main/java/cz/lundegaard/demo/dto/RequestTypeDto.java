package cz.lundegaard.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;

public class RequestTypeDto extends AbstractDto {

    private String name;

    private RequestTypeDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt, String name) {
        super(id, createdAt, updatedAt);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static RequestTypeDto.RequestTypeDtoBuilder builder() {
        return new RequestTypeDto.RequestTypeDtoBuilder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class RequestTypeDtoBuilder {

        private long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private String name;

        public RequestTypeDtoBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public RequestTypeDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public RequestTypeDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public RequestTypeDtoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public RequestTypeDto build() {
            return new RequestTypeDto(id, createdAt, updatedAt, name);
        }
    }
}
