package cz.lundegaard.demo.dto.mapping;

import cz.lundegaard.demo.domain.RequestType;
import cz.lundegaard.demo.dto.RequestTypeDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RequestTypeMapper {

    RequestTypeDto toDto(RequestType requestType);

    RequestType toEntity(RequestTypeDto requestTypeDto);
}
