package cz.lundegaard.demo.dto.mapping;

import cz.lundegaard.demo.domain.ContactRequest;
import cz.lundegaard.demo.dto.ContactRequestDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ContactRequestMapper {

    ContactRequestDto toDto(ContactRequest contactRequest);

    ContactRequest toEntity(ContactRequestDto contactRequestDto);
}
