package cz.lundegaard.demo.dto.mapping;

import cz.lundegaard.demo.domain.Client;
import cz.lundegaard.demo.dto.ClientDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    @Mapping(target = "contactRequest", ignore = true)
    ClientDto toDto(Client contactRequest);

    @Mapping(target = "contactRequest", ignore = true)
    Client toEntity(ClientDto contactRequestDto);
}
