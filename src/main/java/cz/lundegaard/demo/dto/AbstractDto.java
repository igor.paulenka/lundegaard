package cz.lundegaard.demo.dto;

import java.time.LocalDateTime;

public class AbstractDto {

    private long id;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public AbstractDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }
}
