package cz.lundegaard.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;
import java.util.List;

@JsonDeserialize(builder = ClientDto.ClientDtoBuilder.class)
public class ClientDto extends AbstractDto {

    private String policyNumber;
    private String name;
    private String surname;
    private List<ContactRequestDto> contactRequest;

    private ClientDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt, String policyNumber, String name, String surname, List<ContactRequestDto> contactRequest) {
        super(id, createdAt, updatedAt);
        this.policyNumber = policyNumber;
        this.name = name;
        this.surname = surname;
        this.contactRequest = contactRequest;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public List<ContactRequestDto> getContactRequest() {
        return contactRequest;
    }

    public static ClientDtoBuilder builder() {
        return new ClientDtoBuilder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class ClientDtoBuilder {

        private long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private String policyNumber;
        private String name;
        private String surname;
        private List<ContactRequestDto> contactRequest;

        public ClientDtoBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public ClientDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public ClientDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public ClientDtoBuilder setPolicyNumber(String policyNumber) {
            this.policyNumber = policyNumber;
            return this;
        }

        public ClientDtoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public ClientDtoBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public ClientDtoBuilder setContactRequest(List<ContactRequestDto> contactRequest) {
            this.contactRequest = contactRequest;
            return this;
        }

        public ClientDto build() {
            return new ClientDto(id, createdAt, updatedAt, policyNumber, name, surname, contactRequest);
        }
    }
}
