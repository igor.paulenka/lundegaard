package cz.lundegaard.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;

@JsonDeserialize(builder = ContactRequestDto.ContactRequestDtoBuilder.class)
public class ContactRequestDto extends AbstractDto {

    private ClientDto client;
    private String message;
    private String name;
    private String surname;
    private boolean status;
    private RequestTypeDto requestType;


    private ContactRequestDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt, ClientDto client, String message, String name, String surname, boolean status, RequestTypeDto requestType) {
        super(id, createdAt, updatedAt);
        this.client = client;
        this.message = message;
        this.name = name;
        this.surname = surname;
        this.status = status;
        this.requestType = requestType;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public ClientDto getClient() {
        return client;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }

    public RequestTypeDto getRequestType() {
        return requestType;
    }


    public static ContactRequestDtoBuilder builder() {
        return new ContactRequestDtoBuilder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class ContactRequestDtoBuilder {

        private long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private ClientDto client;
        private String message;
        private String name;
        private String surname;
        private boolean status;
        private RequestTypeDto requestType;

        public ContactRequestDtoBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public ContactRequestDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public ContactRequestDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public ContactRequestDtoBuilder setClientDto(ClientDto client) {
            this.client = client;
            return this;
        }

        public ContactRequestDtoBuilder setMessage(String message) {
            this.message = message;
            return this;
        }

        public ContactRequestDtoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public ContactRequestDtoBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public ContactRequestDtoBuilder setStatus(boolean status) {
            this.status = status;
            return this;
        }

        public ContactRequestDtoBuilder setRequestTypeDto(RequestTypeDto requestType) {
            this.requestType = requestType;
            return this;
        }

        public ContactRequestDto build() {
            return new ContactRequestDto(id, createdAt, updatedAt, client, message, name, surname, status, requestType);
        }
    }
}
