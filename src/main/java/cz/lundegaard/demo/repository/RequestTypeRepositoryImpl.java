package cz.lundegaard.demo.repository;

import cz.lundegaard.demo.domain.RequestType;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class RequestTypeRepositoryImpl implements RequestTypeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<RequestType> findAll() {
        Query query = entityManager.createNamedQuery(RequestType.QUERY_ALL, RequestType.class);
        return (List<RequestType>) query.getResultList();
    }

    @Override
    public RequestType create(RequestType requestType) {
        entityManager.persist(requestType);
        return requestType;
    }

    @Override
    public RequestType update(RequestType requestType) {
        entityManager.merge(requestType);
        return requestType;
    }

    @Override
    public void delete(long id) {
        RequestType requestType = entityManager.find(RequestType.class, id);
        entityManager.remove(requestType);
    }
}
