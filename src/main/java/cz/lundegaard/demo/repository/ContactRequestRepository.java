package cz.lundegaard.demo.repository;

import cz.lundegaard.demo.domain.ContactRequest;

import java.util.List;

/**
 * Repository interface with declaration of methods which serves the {@link cz.lundegaard.demo.domain.ContactRequest} class.
 */
public interface ContactRequestRepository {

    /**
     * Method find all contact requests.
     *
     * @return List of all contact requests
     */
    List<ContactRequest> findAll();

    /**
     * Method creates new contact request.
     *
     * @param contactRequest The contact request to be created.
     * @return Created request.
     */
    ContactRequest create(ContactRequest contactRequest);

    /**
     * Method updates existing contact request.
     *
     * @param contactRequest The contact request to be created.
     * @return Updated request.
     */
    ContactRequest update(ContactRequest contactRequest);

    /**
     * Method deletes chosen contact request.
     *
     * @param id Identification parameter (primary key) of contact request. Must not be null or negative.
     */
    void delete(long id);
}
