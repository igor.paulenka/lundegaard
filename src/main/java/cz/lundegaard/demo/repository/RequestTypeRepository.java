package cz.lundegaard.demo.repository;

import cz.lundegaard.demo.domain.RequestType;

import java.util.List;

/**
 * Repository interface with declaration of methods which serves the {@link cz.lundegaard.demo.domain.RequestType} class.
 */
public interface RequestTypeRepository {

    /**
     * Method find all request types.
     *
     * @return List of all request types.
     */
    List<RequestType> findAll();

    /**
     * Method creates new request type.
     *
     * @param requestType The request type to be created.
     * @return Created request type.
     */
    RequestType create(RequestType requestType);

    /**
     * Method updates existing request type.
     *
     * @param requestType The request type to be updated.
     * @return Updated request type.
     */
    RequestType update(RequestType requestType);

    /**
     * Method deletes request type.
     *
     * @param id Identification parameter (primary key) of request type. Must not be null or negative.
     */
    void delete(long id);
}
