package cz.lundegaard.demo.repository;

import cz.lundegaard.demo.domain.Client;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Client> findAll() {
        Query query = entityManager.createNamedQuery(Client.QUERY_ALL, Client.class);
        return (List<Client>) query.getResultList();
    }

    @Override
    public Client create(Client client) {
        entityManager.persist(client);
        return client;
    }

    @Override
    public Client update(Client client) {
        entityManager.merge(client);
        return client;
    }

    @Override
    public void delete(long id) {
        Client client = entityManager.find(Client.class, id);
        entityManager.remove(client);
    }

    @Override
    public long findBy(String policyNumber) {
        final String QUERY = "SELECT id FROM Client WHERE policyNumber = ?1";
        Query query = entityManager.createQuery(QUERY)
                .setParameter(1, policyNumber);
        return (long) query.getSingleResult();
    }
}
