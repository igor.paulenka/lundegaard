package cz.lundegaard.demo.repository;

import cz.lundegaard.demo.domain.ContactRequest;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ContactRequestRepositoryImpl implements ContactRequestRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<ContactRequest> findAll() {
        return entityManager.createNamedQuery(ContactRequest.QUERY_ALL, ContactRequest.class)
                .getResultList();
    }

    @Override
    public ContactRequest create(ContactRequest contactRequest) {
        entityManager.persist(contactRequest);
        return contactRequest;
    }

    @Override
    public ContactRequest update(ContactRequest contactRequest) {
        entityManager.merge(contactRequest);
        return contactRequest;
    }

    @Override
    public void delete(long id) {
        ContactRequest contactRequest = entityManager.find(ContactRequest.class, id);
        entityManager.remove(contactRequest);
    }
}
