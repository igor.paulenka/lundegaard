package cz.lundegaard.demo.service;

import cz.lundegaard.demo.dto.RequestTypeDto;

import java.util.List;

/**
 * Service interface with declaration of methods which serves the {@link cz.lundegaard.demo.domain.RequestType} class.
 */
public interface RequestTypeService {

    /**
     * Method find all request types.
     *
     * @return List of all request types.
     */
    List<RequestTypeDto> findAll();

    /**
     * Method creates new request type.
     *
     * @param requestType The request type to be created.
     * @return Created request type.
     */
    RequestTypeDto create(RequestTypeDto requestType);

    /**
     * Method updates existing request type.
     *
     * @param requestType The request type to be updated.
     * @return Updated request type.
     */
    RequestTypeDto update(RequestTypeDto requestType);

    /**
     * Method deletes request type.
     *
     * @param id Identification parameter (primary key) of request type. Must not be null or negative.
     */
    void delete(long id);
}
