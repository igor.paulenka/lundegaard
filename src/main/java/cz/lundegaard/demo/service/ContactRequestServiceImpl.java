package cz.lundegaard.demo.service;

import cz.lundegaard.demo.domain.ContactRequest;
import cz.lundegaard.demo.dto.ContactRequestDto;
import cz.lundegaard.demo.dto.mapping.ContactRequestMapper;
import cz.lundegaard.demo.repository.ContactRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class ContactRequestServiceImpl implements ContactRequestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactRequestServiceImpl.class);

    private final ContactRequestRepository contactRequestRepository;
    private final ContactRequestMapper contactRequestMapper;

    public ContactRequestServiceImpl(ContactRequestRepository contactRequestRepository, ContactRequestMapper contactRequestMapper) {
        this.contactRequestRepository = contactRequestRepository;
        this.contactRequestMapper = contactRequestMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContactRequestDto> findAll() {
        LOGGER.info("Find all contact requests method was called.");
        List<ContactRequestDto> contactRequestDtos = contactRequestRepository.findAll()
                .stream()
                .map(contactRequestMapper::toDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned contacts request: {}.", contactRequestDtos.size());
        return contactRequestDtos;
    }

    @Override
    public ContactRequestDto create(ContactRequestDto contactRequest) {
        LOGGER.info("Create contact request method was called.");
        ContactRequest entity = contactRequestMapper.toEntity(contactRequest);
        ContactRequest newEntity = contactRequestRepository.create(entity);
        ContactRequestDto dto = contactRequestMapper.toDto(newEntity);
        LOGGER.info("Contact request {} was created.", newEntity);
        return dto;
    }

    @Override
    public ContactRequestDto update(ContactRequestDto contactRequest) {
        ContactRequest entity = contactRequestMapper.toEntity(contactRequest);
        LOGGER.info("Update contact request method was called on request: {}.", entity);
        ContactRequest newEntity = contactRequestRepository.update(entity);
        ContactRequestDto dto = contactRequestMapper.toDto(newEntity);
        LOGGER.info("Contact request was updated to {}.", newEntity);
        return dto;
    }

    @Override
    public void delete(long id) {
        Objects.requireNonNull(id);
        LOGGER.info("Delete contact request method was called on request with ID: {}.", id);
        contactRequestRepository.delete(id);
        LOGGER.info("Contact request with ID {} was deleted.", id);
    }
}
