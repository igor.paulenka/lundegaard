package cz.lundegaard.demo.service;

import cz.lundegaard.demo.dto.ContactRequestDto;

import java.util.List;

/**
 * Service interface with declaration of methods which serves the {@link cz.lundegaard.demo.domain.ContactRequest} class.
 */
public interface ContactRequestService {

    /**
     * Method find all contact requests.
     *
     * @return List of all contact requests.
     */
    List<ContactRequestDto> findAll();

    /**
     * Method creates new contact request.
     *
     * @param contactRequest The contact request to be created.
     * @return Created request.
     */
    ContactRequestDto create(ContactRequestDto contactRequest);

    /**
     * Method updates existing contact request.
     *
     * @param contactRequest The contact request to be updated.
     * @return Updated request.
     */
    ContactRequestDto update(ContactRequestDto contactRequest);

    /**
     * Method deletes chosen company request.
     *
     * @param id Identification parameter (primary key) of contact request. Must not be null or negative.
     */
    void delete(long id);
}
