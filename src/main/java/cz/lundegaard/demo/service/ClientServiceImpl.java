package cz.lundegaard.demo.service;

import cz.lundegaard.demo.domain.Client;
import cz.lundegaard.demo.dto.ClientDto;
import cz.lundegaard.demo.dto.mapping.ClientMapper;
import cz.lundegaard.demo.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;

    public ClientServiceImpl(ClientRepository clientRepository, ClientMapper clientMapper) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ClientDto> findAll() {
        LOGGER.info("Find all clients method was called.");
        List<ClientDto> clientDtoList = clientRepository.findAll()
                .stream()
                .map(clientMapper::toDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned clients: {}.", clientDtoList.size());
        return clientDtoList;
    }

    @Override
    public ClientDto create(ClientDto client) {
        LOGGER.info("Create client method was called.");
        Client entity = clientMapper.toEntity(client);
        Client newEntity = clientRepository.create(entity);
        ClientDto dto = clientMapper.toDto(newEntity);
        LOGGER.info("Client {} was created.", newEntity);
        return dto;
    }

    @Override
    public ClientDto update(ClientDto client) {
        Client entity = clientMapper.toEntity(client);
        LOGGER.info("Update client method was called on clients: {}.", entity);
        Client newEntity = clientRepository.update(entity);
        ClientDto dto = clientMapper.toDto(newEntity);
        LOGGER.info("Client was updated to {}.", newEntity);
        return dto;
    }

    @Override
    public void delete(long id) {
        Objects.requireNonNull(id);
        LOGGER.info("Delete clients method was called on clients with ID: {}.", id);
        clientRepository.delete(id);
        LOGGER.info("Client with ID {} was deleted.", id);
    }

    @Override
    @Transactional(readOnly = true)
    public long findBy(String policyNumber) {
        LOGGER.info("Method find client ID by policy number: {} was called.", policyNumber);
        long id = clientRepository.findBy(policyNumber);
        LOGGER.info("Method find client by policy number found client with ID: {}.", id);
        return id;
    }
}
