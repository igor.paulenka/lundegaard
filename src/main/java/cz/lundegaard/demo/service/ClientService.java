package cz.lundegaard.demo.service;

import cz.lundegaard.demo.dto.ClientDto;

import java.util.List;

/**
 * Service interface with declaration of methods which serves the {@link cz.lundegaard.demo.domain.Client} class.
 */
public interface ClientService {

    /**
     * Method find all clients.
     *
     * @return List of all clients.
     */
    List<ClientDto> findAll();

    /**
     * Method creates new client.
     *
     * @param client The client to be created.
     * @return Created client.
     */
    ClientDto create(ClientDto client);

    /**
     * Method updates existing client.
     *
     * @param client The client to be updated.
     * @return Updated client.
     */
    ClientDto update(ClientDto client);

    /**
     * Method deletes client.
     *
     * @param id Identification parameter (primary key) of client. Must not be null or negative.
     */
    void delete(long id);

    /**
     * Method finds client by his policy number.
     *
     * @param policyNumber Identification parameter. Must not be null.
     * @return Id of founded client.
     */
    long findBy(String policyNumber);
}
