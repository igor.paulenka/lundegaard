package cz.lundegaard.demo.service;

import cz.lundegaard.demo.domain.RequestType;
import cz.lundegaard.demo.dto.RequestTypeDto;
import cz.lundegaard.demo.dto.mapping.RequestTypeMapper;
import cz.lundegaard.demo.repository.RequestTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class RequestTypeServiceImpl implements RequestTypeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestTypeServiceImpl.class);

    private final RequestTypeRepository requestTypeRepository;
    private final RequestTypeMapper requestTypeMapper;

    public RequestTypeServiceImpl(RequestTypeRepository requestTypeRepository, RequestTypeMapper requestTypeMapper) {
        this.requestTypeRepository = requestTypeRepository;
        this.requestTypeMapper = requestTypeMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequestTypeDto> findAll() {
        LOGGER.info("Find all request types method was called.");
        List<RequestTypeDto> requestTypeDtos = requestTypeRepository.findAll()
                .stream()
                .map(requestTypeMapper::toDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned request types: {}.", requestTypeDtos.size());
        return requestTypeDtos;
    }

    @Override
    public RequestTypeDto create(RequestTypeDto requestType) {
        LOGGER.info("Create request type method was called.");
        RequestType entity = requestTypeMapper.toEntity(requestType);
        RequestType newEntity = requestTypeRepository.create(entity);
        RequestTypeDto dto = requestTypeMapper.toDto(newEntity);
        LOGGER.info("Request type {} was created.", newEntity);
        return dto;
    }

    @Override
    public RequestTypeDto update(RequestTypeDto requestType) {
        RequestType entity = requestTypeMapper.toEntity(requestType);
        LOGGER.info("Update request type method was called on request: {}.", entity);
        RequestType newEntity = requestTypeRepository.update(entity);
        RequestTypeDto dto = requestTypeMapper.toDto(newEntity);
        LOGGER.info("Request type was updated to {}.", newEntity);
        return dto;
    }

    @Override
    public void delete(long id) {
        Objects.requireNonNull(id);
        LOGGER.info("Delete request type method was called on request with ID: {}.", id);
        requestTypeRepository.delete(id);
        LOGGER.info("Request type with ID {} was deleted.", id);
    }
}
