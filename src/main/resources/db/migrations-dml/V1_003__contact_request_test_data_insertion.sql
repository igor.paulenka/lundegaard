INSERT INTO CONTACT_REQUEST (CREATED_AT, LAST_UPDATE, MESSAGE, NAME, SURNAME, STATUS, REQUEST_TYPE_ID, CLIENT_ID)
VALUES (now(), now(), 'First Message', 'FirstName', 'FirstSurname', false, 1, 1),
       (now(), now(), 'Second Message', 'SecondName', 'SecondSurname', false, 3, 2);