INSERT INTO REQUEST_TYPE (CREATED_AT, LAST_UPDATE, NAME)
VALUES (now(), now(), 'Contract Adjustment'),
       (now(), now(), 'Damage Case'),
       (now(), now(), 'Complaint');
