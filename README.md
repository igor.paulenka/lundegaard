# Application LundegaardDemo
Application LundegaardDemo is currently deployed and running on [Heroku Cloud](https://lundegaard-dev-demo.herokuapp.com/).

### Table of content
1.  [Built With](#built-with)
    *   [External Tools Used](#external-tools-used)
2.  [Actuator](#actuator)
3.  [Features](#features)
4.  [Endpoints](#endpoints)
    *   [Status codes](#status-codes)
5.  [Installing](#installing)
6.  [Deploying](#deploying)

## Built With
* 	[Maven](https://maven.apache.org/) - Dependency Management
* 	[Flyway](https://flywaydb.org/) - Version control for database
* 	[JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java™ Platform, Standard Edition Development Kit 
* 	[Spring Boot](https://spring.io/projects/spring-boot) - Framework to ease the bootstrapping and development of new Spring Applications
* 	[PostgreSQL](https://www.postgresql.org/) - Open-Source Object-Relational Database System
* 	[git](https://git-scm.com/) - Free and Open-Source distributed version control system 
*   [Vue.js](https://vuejs.org/) - Open-Source JavaScript framework with various optional tools for building user interfaces.
*   [axios](https://github.com/axios/axios) -  HTTP client for the browser and node.js
### External Tools Used
* [Postman](https://www.getpostman.com/) - API Development Environment

## Actuator
To monitor and manage application are available this actuators:

|  URL                                                                  |Method |
|-----------------------------------------------------------------------|:-----:|
|`https://lundegaard-dev-demo.herokuapp.com/actuator`                   | `GET` |
|`https://lundegaard-dev-demo.herokuapp.com/actuator/health`    	    | `GET` |
|`https://lundegaard-dev-demo.herokuapp.com/actuator/info`      	    | `GET` |
|`https://lundegaard-dev-demo.herokuapp.com/actuator/env`               | `GET` |
|`https://lundegaard-dev-demo.herokuapp.com/actuator/heapdump`          | `GET` |

*Credentials for access are:*
*   login: **appAdmin**
*   password: **appPassword**

## Features
Application define CRUD operations for three entities.
*   Client
*   Request Type
*   Contact Request

This entities have relationship model define as:

`Client` 1 <=> n `Contact Request` 1 <=> 1 `Request Type`

As default index file run simple [Contact Us](https://lundegaard-dev-demo.herokuapp.com/) form.

With following fields:
*   Kind of request: 

Contain select menu of `Request Type` populated form database through `get` method of RequestTypeController endpoint `/requestType/name`.
*   Policy Number: 
   
A text field that could contain up to 9 alphanumeric characters. 
Inserted value must be stored in database table owned by `Client` entity.
*   Name: 
   
A text field that could contain up to 20 alphabetic characters.
*   Surname: 
   
A text field that could contain up to 20 alphabetic characters.
*   Your request:

Message body that could contain up to 2500 characters.

After sending is request processed and saved into the database. 
For communication with BE is used `/ContactRequest/contactUs` endpoint.
## Endpoints
For communication with endpoints must be `Content-type` as `application/json` specified.
Assessable endpoints are:

|       |Endpoint                                                                 |Request type                    |
|:-----:|-------------------------------------------------------------------------|--------------------------------|
|1      | [/client/](https://lundegaard-dev-demo.herokuapp.com/)                        |`GET`, `PUT`, `POST` or `DELETE`|
|2      | [/contactRequest/](https://lundegaard-dev-demo.herokuapp.com/contactRequest)  |`GET`, `PUT`, `POST` or `DELETE`|
|3      | [/requestType/](https://lundegaard-dev-demo.herokuapp.com/requestType)        |`GET`, `PUT`, `POST` or `DELETE`|


Basic information for communication with REST API for entities are listed in following tables.
*   Entity: **Client**

|Request type| Endpoint | Request Parameter|Returned|
|:---:|---|:---:|---|
|`GET`|`/client`| |List of all clients|
|`POST`|`/client`| |Created client|
|`PUT`|`/client`| |Updated client|
|`DELETE`|`/client`|`id`| |

`id` - primary key of client

*   Entity: **Request Type**

|Request type| Endpoint | Request Parameter|Returned|
|:---:|---|:---:|---|
|`GET`|`/contactRequest`|  |All available requests|
|`POST`|`/contactRequest`| |Created request type|
|`PUT`|`/contactRequest`| |Updated request type|
|`DELETE`|`/contactRequest`|`id`| |

`id` - primary key of company branch entity

*   Entity: **Contact Request**

|Request type| Endpoint | Request Parameter|Returned|
|:---:|---|:---:|---|
|`GET`|`/requestType`| |List of all contact requests|
|`POST`|`/requestType`| |Created contact request|
|`PUT`|`/requestType`| |Updated contact request|
|`DELETE`|`/requestType`|`id`| |

`id` - primary key of request type entity

More information and templates for basic requests are available in [REST_API_TEMPLATE](REST_API_TEMPLATE.md) file.

### Status codes

The API is designed to return different status codes according to context and action. 
This way, if a request results in an error, the caller is able to get insight into what went wrong.

The following table gives an overview of how the API functions behave.

| Request type | Description |
| ------------ | ----------- |
| `GET`   | Access one or more resources and return the result as JSON. |
| `POST`  | Return `201 Created` if the resource is successfully created and return the newly created resource as JSON. |
| `GET` / `PUT` | Return `200 OK` if the resource is accessed or modified successfully. The (modified) result is returned as JSON. |
| `DELETE` | Returns `204 No Content` if the resource was deleted successfully. |

The following table shows the possible return codes for API requests.

| Return values | Description |
| ------------- | ----------- |
| `200 OK` | The `GET`, `PUT` request was successful, the resource(s) itself is returned as JSON. |
| `204 No Content` | The server has successfully fulfilled the  `DELETE` request and that there is no additional content to send in the response payload body. |
| `201 Created` | The `POST` request was successful and the resource is returned as JSON. |
| `400 Bad Request` | A required attribute of the API request is missing. |
| `404 Not Found` | A resource could not be accessed, e.g., an ID for a resource could not be found. |
| `500 Server Error` | While handling the request something went wrong server-side. |

## Installing
Application is available on [gitlab](https://gitlab.com/igor.paulenka/lundegaard). 
Public access for app allows run her locally by clone git repository:
  
  `git clone https://gitlab.com/igor.paulenka/lundegaard.git`

For run can be used by external tools (Intellij IDEA, Eclipse,..) or execution:
 
  ```shell
  mvn spring-boot:run
```
Application run by default with active profile `dev`. 
Properties for this profile is located inside files:

 `src/main/resources/application.properties`
 
 `src/main/resources/application-localhost.properties`
 
Profile `it` served for integration tests of repository layer during verify phase. 
For tests is used external database on [elephantsql.com](https://www.elephantsql.com/).
Profile properties is located in file:

`src/test/resources/application-it.properties`

## Deploying
Application is automatic deploy on heroku cloud by committing into the master branch through gitlab CI. 
Script for this deploying is located in `.gitlab-ci.yml` file. 
For deployment active profile `prod` is choosed by command insade `Procfile`. 
Properties for this profile is located inside files:

 ``src/main/resources/application.properties``
 
 ``src/main/resources/application-heroku.properties`` 

Profile `it` served for integration tests of repository layer during verify phase. 
For tests is used external database on [elephantsql.com](https://www.elephantsql.com/). 
Profile properties is located in file:

``src/test/resources/application-it.properties``

