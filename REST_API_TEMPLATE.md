# REST API

This file contain basics templates for communication with REST API. This requests could be modified. 
Request and response variables can be changed by modifying `MapStruct` settings in `mapping` package.

If application run locally endpoins are accessable by default on:

`https://localhost:8080/{ENDPOINTS}`

If run on heroku cloud url for endpoints is in form:

`https://lundegaard-dev-demo.herokuapp.com/{ENDPOINTS}`

Possible ENDPOINTS are: 
1.  [client](#client)
2.  [contactRequest](#contactRequest)
3.  [requestType](#requestType)

  
## client
#### GET
Response for `GET` request return all saved clients in `json` format:
```json
[
    {
        "id": 1,
        "createdAt": "2020-02-07T10:48:46.407571",
        "updatedAt": "2020-02-07T10:48:46.407571",
        "policyNumber": "0123456789",
        "name": "Anthony",
        "surname": "Alons",
        "contactRequest": null
    },
    ...
]
```
#### POST
`POST` request saved new client into the database.
Template for request in `json` format is:
```json
{
    "policyNumber": "s",
    "name": "s",
    "surname": "s"
}
```
Response for `POST` request return created client in `json` format with generated `id` and *timestamps*:
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "policyNumber": "",
    "name": "",
    "surname": "",
    "contactRequest": null
}
```
#### PUT
`PUT` request update client in the database. Request must contain client `name`, `surname` and `policyNumber`. 
Template for request in ``json`` is:
```json
{
    "id": 1,
    "policyNumber": "",
    "name": "",
    "surname": ""
}
```
Response for `PUT` request returned updated information about client in `json` format:
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "policyNumber": "",
    "name": "",
    "surname": "",
    "contactRequest": null
}
```
#### DELETE
Request `DELETE` required `id` of client, that to be deleted as request parameter. 
## contactRequest
#### GET
Response for `GET` request return all contact requests in `json` format:
```json
[
    {
        "id": 1,
        "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "client": null,
        "message": "",
        "name": "",
        "surname": "",
        "status": false,
        "requestType": null
    },
    ...
]
```
#### POST
`POST` request must contain `name`, `surname`, `message` of new contact request.
 Template for request in `json` format is:
```json
{
    "client": null,
    "message": "",
    "name": "",
    "surname": "",
    "status": false,
    "requestType": null
}
```
Response for `POST` request contain returned contact request in `json` format with generated *timestamps* and `id`:
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "client": null,
    "message": "",
    "name": "",
    "surname": "",
    "status": false,
    "requestType": null
}
```
#### PUT
`PUT` request must contain `id` which is primary keys of contact request. `name`, `surname`, and `message` fields must not be null. 
Template for `PUT` request in `json` format is:
```json
{
    "id": 1,
    "client": null,
    "message": "",
    "name": "",
    "surname": "",
    "status": false,
    "requestType": null
}
```
Response for `PUT` request returned updated contact request. 
Returned `json` format response is:
```json
{
    "id": 1,
    "createdAt": null,
    "updatedAt": null,
    "client": null,
    "message": "",
    "name": "",
    "surname": "",
    "status": false,
    "requestType": null
}
```
#### DELETE
Request `DELETE` must contain requested parameter `id` which is primary key of contact request that to be deleted.
## requestType
#### GET
1. Request without arguments
`GET` request returned as response list of all request types saved in database. 
Response in `json` format for this requests is:
```json
[
    {
        "id": 1,
        "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "name": ""
    },
    ...
]
```
#### POST
`POST` request must contain request type `name`.
Template for this request in `json` format is: 
 ```json
{
    "name": ""
}
```
Response for `POST` request returned created request type with generated *timestamp* and *primary key* in `json` format. 
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "name": ""
}
```
#### PUT
`PUT` request must contain *primary key* of request type that to be updated and request name. 
Template for this request in `json` format is:
```json
{
    "id": 1,
    "name": ""
}
```
Response for `PUT` request contain updated information in `json` format:
```json
{
    "id": 1,
    "createdAt": null,
    "updatedAt": null,
    "name": ""
}
```
#### DELETE
Request `DELETE` must contain requested parameter `id` which is primary key of request type that to be deleted.
